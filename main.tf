
#VPC
resource "aws_vpc" "test-auto-vpc" {
  cidr_block       = "172.16.0.0/24"
  instance_tenancy = "default"
  tags = {
    Name = "test-auto"
  }
}

#Subnet
resource "aws_subnet" "test-auto-subnet" {
  vpc_id     = aws_vpc.test-auto-vpc.id
  cidr_block = "172.16.0.0/24"

  tags = {
    Name = "test-auto"
  }
}

#Internet Gateway
resource "aws_internet_gateway" "test-auto-igw" {
  vpc_id = aws_vpc.test-auto-vpc.id

  tags = {
    Name = "test-auto"
  }
}

#RoutTable
resource "aws_route_table" "test-auto-rt" {
  vpc_id = aws_vpc.test-auto-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test-auto-igw.id
  }

  tags = {
    Name = "test-auto"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.test-auto-subnet.id
  route_table_id = aws_route_table.test-auto-rt.id
}

#KeyPair
variable "public_key_path" {}
resource "aws_key_pair" "deployer" {
  key_name   = "test-auto-key"
  public_key = file(var.public_key_path)
}


#Security Group
resource "aws_security_group" "test-auto-sg" {
  description = "test-auto-sg"
  name        = "test-auto-sg"
  vpc_id      = aws_vpc.test-auto-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#VPC
resource "aws_network_interface" "test-auto-nic" {
  subnet_id = aws_subnet.test-auto-subnet.id
  security_groups = [
    aws_security_group.test-auto-sg.id
  ]
  attachment {
    instance     = aws_instance.test-auto-vpc.id
    device_index = 1
  }

  tags = {
    Name = "test-auto"
  }
}

resource "aws_instance" "test-auto-vpc" {
  subnet_id = aws_subnet.test-auto-subnet.id
  security_groups = [
    aws_security_group.test-auto-sg.id
  ]
  ami                                  = "ami-0af1df87db7b650f4"
  key_name                             = aws_key_pair.deployer.key_name
  instance_type                        = "t2.micro"
  tenancy                              = "default"
  monitoring                           = false
  disable_api_termination              = false
  instance_initiated_shutdown_behavior = "stop"
  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "test-auto"
  }

  ebs_optimized = false
  root_block_device {
    volume_type = "gp2"
    volume_size = 30
  }
}

#EIP
resource "aws_eip" "test-auto-eip" {
  network_interface = aws_network_interface.test-auto-nic.id
  vpc      = true
}