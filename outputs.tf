output "vpc_id" {
  description = "The ID of the VPC"
  value       = concat(aws_vpc.test-auto-vpc.*.id, [""])[0]
}

output "vpc_arn" {
  description = "The ARN of the VPC"
  value       = concat(aws_vpc.test-auto-vpc.*.arn, [""])[0]

}

output "aws_subnet" {
  description = "The ID of the Subnet"
  value       = concat(aws_subnet.test-auto-subnet.*.id, [""])[0]
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = concat(aws_subnet.test-auto-subnet.*.cidr_block, [""])[0]
}

output "aws_security_group_id" {
  description = "The ID of the security group on VPC creation"
  value       = concat(aws_security_group.test-auto-sg.*.name, [""])[0]
}

output "aws_eip" {
  description = "The IP of the VPC"
  value       = concat(aws_eip.test-auto-eip.*.public_ip, [""])[0]
}