# README #

Terraform Sample Make EC2 For Test.
Terraformを使用し、テスト用のEC2サーバーをセットアップします。

・VPC、
・サブネット
・ルーティング
・デフォルトゲートウェイ
・セキュリティグループ(22番ポート)
・EC2（t2.micro1台）


### What is this repository for? ###

前提
* Terraform Version:Terraform v0.12.20
+ provider.aws v2.48.0

* Terraformのコマンドを実行する環境にて、
  AWS CLIが設定されていること


### How do I get set up? ###

EC2接続用にSSHの公開鍵を設定しています。
事前にご用意のうえ、「terraform.tfvars」ファイルの
「publi_key_path」にパスとファイル名をご指定ください。

* make folder "key" at root folder.
* make ssh key for connect EC2 and save to key folder.
* modify terraform.tfvars in publi_key_path to your ssh key name.
  
プロジェクトのパスに移動し、コマンドを実行します。
terraform initは初回のみ実行して下さい。

* terraform init
* terraform plan
  　→問題が無いことを確認します。

* terraform apply
  　→terraform planに問題が無い場合に実行してください。

* terraform destroy
    →作成した環境を削除する場合は、こちらを実行してください。

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact